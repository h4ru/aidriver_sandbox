import os, sys, glob
sys.path.insert(0, os.path.join(os.path.dirname(__file__)+'/control'))
sys.path.insert(0, os.path.join(os.path.dirname(__file__)+'/plot_tools'))
sys.path.insert(0, os.path.join(os.path.dirname(__file__)+'/plot_tools'))

from mpc_kinematic_controller_f import mpc_kin_controller
from car_plot import plot_car

import numpy as np
import matplotlib.pyplot as plt

import time
# ------------------------------------------------------------------------------------
SAVE_PLOTS = False

# ###################################################################
# NLP problem definitions
#  - Time horizon (th)
#  - Discretization parameters (m)
#  - Number of discretization points (n)
######################################################################
m = 8 # discretization steps for the Runge-Kutta integrator
th =30
n = int(th*4)


#######################################################################
# Setpoints
#######################################################################

# Polynomial curve x = f(y)
a = -0.00017281974398982732
b = 0.008487188059584268
c = 0.07486318249450581
d = -0.6105759773120379

v_cruise = 50/3.6 # m/s desired speed along the trajectory (soft constraint)
vf = 0 # desired final speed (soft constraint)
sf = 100  # desired trajectory length


setpoints = [a, b, c, d, v_cruise, vf, sf]

#######################################################################
# Model predictive controller weights
#######################################################################
kSteer = 1
kDeltadot = 1
kSpeed = 0.1
kThrottle = 1
qxte = 10
qtheta_e = 1
Pxte = 10
Ptheta_e = 10
Pvf = 20
Psf = 10

cost_weights = [kSteer, kDeltadot, kSpeed, kThrottle, qxte, qtheta_e, Pxte, Ptheta_e, Pvf, Psf]

#######################################################################
# Vehicle model constraints
#######################################################################

# servo-steering limits
delta_angle_limit = (45 / 180) * np.pi  # degrees
delta_rate_limit = (180 / 180) * np.pi  # degrees per second
# Speed limits
speed_limit = 100/3.6
throttle_limit = 1
jerk_limit = 0.5

limits = [delta_angle_limit, delta_rate_limit, speed_limit, throttle_limit, jerk_limit, sf]

# Car geometry and physical limits
Lr = 1.25  # m
Lf = 1.25  # m
car_dimensions = [Lr, Lf]

#######################################################################
# Creation of NLP problem by discretization of the optimal problem
#######################################################################

# Define car model
start_time = time.time()
controller = mpc_kin_controller(th, cost_weights, limits, car_dimensions, n, m)
print("--- NLP generation: %s seconds ---" % (time.time() - start_time))

n = controller.opti.model.n
n_x = controller.opti.model.n_x
n_u = controller.opti.model.n_u

#######################################################################
# Initial conditions for optimal control problem
#######################################################################

pos_x_0 = 0  # f(y) when y = 0
pos_y_0 = 0
theta_0 = np.pi / 2  # car looking ahead
xte_0 = +d
theta_e_0 = -theta_0 + np.arctan2(1, c)
delta_0 = 0
speed_0 = 0
delta_dot_0 = 0
throttle_0 = 0
s_0 = 0

x0 = [pos_x_0, pos_y_0, theta_0, xte_0, theta_e_0, delta_0, speed_0, throttle_0, s_0]

#######################################################################
# Computation of the solution using an optimization solver (Ipopt)
#######################################################################

start_time = time.time()

x_t1, info = controller.update(x0, setpoints)

print("--- NMPC solution: %s seconds ---" % (time.time() - start_time))


ipopt_data = x_t1.reshape(n, (n_x + n_u), order='F')
states_x = ipopt_data[:, 0:n_x]
control_u = ipopt_data[:, n_x:]

pos_x = states_x[:,0]
pos_y = states_x[:,1]
theta = states_x[:,2]
xte = states_x[:,3]
theta_e = states_x[:,4]
delta = states_x[:,5]
speed = states_x[:,6]
throttle = states_x[:,7]
s = states_x[:,8]

delta_dot = control_u[:,0]
jerk = control_u[:,1]

theta_s = np.arctan2(1, 3*a*pos_y**2 + 2*b*pos_y + c)

# ------------------------------------------------------------------------------------

#######################################################################
# Plotting
#######################################################################

plt.figure(1)
poly_coeffs = [a, b, c, d]
curve_poly = np.poly1d(poly_coeffs)
y_curve = np.linspace(min(pos_y), max(pos_y), len(pos_x))
x_curve = curve_poly(y_curve)

if SAVE_PLOTS:
    try:
        os.mkdir('mpc_kin_plots')
    except:
        files = glob.glob('mpc_kin_plots/*.png')
        for f in files:
            os.remove(f)

tick = 0
for state in states_x:
    plt.cla()
    plt.plot(x_curve, y_curve, '--', markersize=2, color='red', label='reference')
    plt.gca().set_aspect('equal', adjustable='box')
    plot_car(-state[0], state[1], np.pi-state[2], -state[5])

    plt.text(-35, 60, "Accel   : " + "{:5.2f} m/s**2".format(state[7]), fontsize=10)
    plt.text(-35, 55, "Speed   : " + "{:5.2f} m/s".format(state[6]), fontsize=10)
    plt.text(-35, 50, "Steering: " + "{:5.2f}°".format(-state[5]*180/np.pi), fontsize=10)


    if SAVE_PLOTS:
        plt.savefig('mpc_kin_plots' + "/fig%04d.png" % tick)
        tick+=1
    else:
        plt.pause(0.05)

#######################################################################

fig1, ax0 = plt.subplots()
ax0.plot(x_curve, y_curve, '--', markersize=2, color='red', label='reference')
ax0.plot(-pos_x, pos_y, 's', markersize = 4, color = 'blue', label='mpc_trajectory')
ax0.set_aspect('equal', adjustable='box')
ax0.grid(color='grey', linestyle='--', linewidth=1)
ax0.legend()

fig2, (ax1, ax2, ax3, ax4, ax5, ax6, ax7) = plt.subplots(7,1)
ax1.plot(np.arange(n)*(th/n),theta_e/np.pi*180, 'b--', label='theta_e')
ax1.legend(shadow=True, fancybox=True)
ax2.plot(np.arange(n)*(th/n),s, 'c--', label='arc length')
ax2.legend(shadow=True, fancybox=True)
ax3.plot(np.arange(n)*(th/n),xte, 'r--', label='cross track error')
ax3.legend(shadow=True, fancybox=True)
ax4.step(np.arange(n)*(th/n),delta/np.pi*180, 'g-', where='post', label='delta')
ax4.legend(shadow=True, fancybox=True)
ax5.step(np.arange(n)*(th/n),speed, 'b-', where='post', label='speed')
ax5.legend(shadow=True, fancybox=True)
ax6.step(np.arange(n)*(th/n),throttle, 'r-', where='post', label='accel')
ax6.legend(shadow=True, fancybox=True)
ax7.step(np.arange(n)*(th/n),jerk, 'm-', where='post', label='jerk')
ax7.legend(shadow=True, fancybox=True)


plt.show()

plt.close()
