import os, sys
sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..', 'vehicle_models'))

from kinematic_model_f import kinematic_model
import numpy as np
import ipopt

# ------------------------------------------------------------------------------------

class mpc_kin_controller:

    def __init__(self, th, cost_weights, limits, car_dimensions, n=10, m=1):
        # time horizon
        self.th = th  # seconds

        # number of steps in time horizon. dt = time_horizon/n
        self.n = n

        # Runge kutta discretization points. h = dt/m (adjust for stability)
        self.m = m

        [delta_angle_limit, delta_rate_limit, speed_limit, throttle_limit, jerk_limit, sf_limit] = limits
        weights = cost_weights

        # (pos_x, pos_y, theta, xte, theta_e, delta, v)
        self.lb_pos_x = self.lb_pos_y = self.lb_theta = self.lb_theta_e = self.lb_xte = -2e19
        self.ub_pos_x = self.ub_pos_y = self.ub_theta = self.ub_theta_e = self.ub_xte = +2e19
        self.lb_delta = -delta_angle_limit
        self.ub_delta = +delta_angle_limit
        self.lb_delta_dot = -delta_rate_limit
        self.ub_delta_dot = +delta_rate_limit

        if sf_limit < 0:
            self.lb_s = sf_limit
            self.ub_s = 0
        else:
            self.lb_s = 0
            self.ub_s = sf_limit

        # (deltadot, throttle)
        if speed_limit < 0:
            self.lb_speed = speed_limit
            self.ub_speed = 0.1
            self.lb_accel = -throttle_limit
            self.ub_accel = 2*throttle_limit # braking
        else:
            self.lb_speed = -0.1
            self.ub_speed = speed_limit
            self.lb_accel = -2*throttle_limit # braking
            self.ub_accel =  throttle_limit


        self.lb_jerk = -jerk_limit
        self.ub_jerk = +jerk_limit


        # Define car model
        model = kinematic_model(self.th,
                                     car_dimensions,
                                     weights,
                                     self.n,
                                     self.m)


        self.opti = self.IpoptOptiProblem(model)
        self.n_vars = self.opti.model.n_vars
        self.n = self.opti.model.n
        self.n_x = self.opti.model.n_x
        self.m_constraints = (self.n - 1)*self.n_vars


    def update(self, x0, setpoints):

        cl = np.zeros((self.n - 1) * self.n_x)
        cu = np.zeros((self.n - 1) * self.n_x)


        [pos_x_0, pos_y_0, theta_0, xte_0, theta_e_0, delta_0, speed_0, accel_0, s_0] = x0

        lb = np.concatenate((np.append(pos_x_0, np.ones(self.n - 1) * self.lb_pos_x),
                             np.append(pos_y_0, np.ones(self.n - 1) * self.lb_pos_y),
                             np.append(theta_0, np.ones(self.n - 1) * self.lb_theta),
                             np.append(xte_0, np.ones(self.n - 1) * self.lb_xte),
                             np.append(theta_e_0, np.ones(self.n - 1) * self.lb_theta_e),
                             np.append(delta_0, np.ones(self.n - 1) * self.lb_delta),
                             np.append(speed_0, np.ones(self.n - 1) * self.lb_speed),
                             np.append(accel_0, np.ones(self.n - 1) * self.lb_accel),
                             np.append(s_0, np.ones(self.n - 1) * self.lb_s),
                             np.ones(self.n) * self.lb_delta_dot,
                             np.ones(self.n) * self.lb_jerk),
                            axis=None)

        ub = np.concatenate((np.append(pos_x_0, np.ones(self.n - 1) * self.ub_pos_x),
                             np.append(pos_y_0, np.ones(self.n - 1) * self.ub_pos_y),
                             np.append(theta_0, np.ones(self.n - 1) * self.ub_theta),
                             np.append(xte_0, np.ones(self.n - 1) * self.ub_xte),
                             np.append(theta_e_0, np.ones(self.n - 1) * self.ub_theta_e),
                             np.append(delta_0, np.ones(self.n - 1) * self.ub_delta),
                             np.append(speed_0, np.ones(self.n - 1) * self.ub_speed),
                             np.append(accel_0, np.ones(self.n - 1) * self.ub_accel),
                             np.append(s_0, np.ones(self.n - 1) * self.ub_s),
                             np.ones(self.n) * self.ub_delta_dot,
                             np.ones(self.n) * self.ub_jerk),
                            axis=None)

        opti_solver_kinematic = ipopt.problem(
            n=self.n_vars,
            m=(self.n-1)*self.n_x,
            problem_obj=self.opti,
            lb=lb,
            ub=ub,
            cl=cl,
            cu=cu
        )

        # opti_solver_kinematic.addOption('mehrotra_algorithm', 'yes')
        # opti_solver_kinematic.addOption('linear_solver', 'ma57')
        opti_solver_kinematic.addOption('mu_strategy', 'adaptive')
        opti_solver_kinematic.addOption('tol', 1e-1)
        opti_solver_kinematic.addOption('acceptable_tol', 1e-1)
        opti_solver_kinematic.addOption('acceptable_iter', 0)
        # opti_solver_kinematic.addOption('warm_start_init_point', 'yes')  # it seems that without warm-start the solution converges faster
        opti_solver_kinematic.addOption('hessian_approximation', 'limited-memory')
        # opti_solver_kinematic.addOption('max_iter', 100)
        # opti_solver_kinematic.addOption('max_cpu_time', 1.0)
        # opti_solver_kinematic.addOption('derivative_test', 'second-order')
        opti_solver_kinematic.addOption('print_level', 0) # zero verbosity

        vars0 = np.concatenate((np.ones(self.n)*pos_x_0,
                                np.ones(self.n)*pos_y_0,
                                np.ones(self.n)*theta_0,
                                np.ones(self.n)*xte_0,
                                np.ones(self.n)*theta_e_0,
                                np.ones(self.n)*delta_0,
                                np.ones(self.n)*speed_0,
                                np.ones(self.n) * accel_0,
                                np.zeros(self.n),  # s
                                np.zeros(self.n),
                                np.zeros(self.n)), #  jerk
                               axis=None)

        # update curve and cruise speed setpoints
        self.opti.update_setpoints(setpoints)

        return opti_solver_kinematic.solve(vars0)


    # ------------------------------------------------------------------------------------

    class IpoptOptiProblem(object):

        def __init__(self, nlp):
            self.model = nlp

        def update_setpoints(self, setpoints):
            self.setpoints = setpoints

        def objective(self, x):
            ipopt_data = x.reshape(self.model.n, (self.model.n_x + self.model.n_u), order='F')
            states_x = ipopt_data[:, 0:self.model.n_x]
            control_u = ipopt_data[:, self.model.n_x:]
            return self.model.Jf(states_x, control_u, self.setpoints)

        def gradient(self, x):
            ipopt_data = x.reshape(self.model.n, (self.model.n_x + self.model.n_u), order='F')
            states_x = ipopt_data[:, 0:self.model.n_x]
            control_u = ipopt_data[:, self.model.n_x:]
            temp_ = np.asarray(self.model.gradJf(states_x, control_u, self.setpoints))
            return temp_.reshape((temp_.shape[0] * temp_.shape[1]), 1, order='F')

        def constraints(self, x):
            ipopt_data = x.reshape(self.model.n, (self.model.n_x + self.model.n_u), order='F')
            states_x = ipopt_data[:, 0:self.model.n_x]
            control_u = ipopt_data[:, self.model.n_x:]
            temp_ = np.asarray(self.model.Gf(states_x, control_u, self.setpoints))
            return temp_.reshape((self.model.n_x * (self.model.n - 1)), 1, order='F')

        def jacobianstructure(self):
            global jacobianG_col, jacobianG_row
            jacobianG_row = self.model.jacobianG.sparsity().get_ccs()[1]
            jacobianG_col = self.model.jacobianG.sparsity().get_col()

            return jacobianG_row, jacobianG_col

        def jacobian(self, x):
            ipopt_data = x.reshape(self.model.n, (self.model.n_x + self.model.n_u), order='F')
            states_x = ipopt_data[:, 0:self.model.n_x]
            control_u = ipopt_data[:, self.model.n_x:]
            jacobianGf_np = np.asarray(self.model.jacobianGf(states_x, control_u, self.setpoints))
            return jacobianGf_np[jacobianG_row,jacobianG_col]
            # return self.model.jacobianGf(states_x, control_u, self.setpoints)

        def hessianstructure(self):
            global hessLag_col, hessLag_row
            hessLag_row = self.model.hessLag.sparsity().get_ccs()[1]
            hessLag_col = self.model.hessLag.sparsity().get_col()
            return hessLag_row, hessLag_col

        # not used, Hessian approx is faster and accurate enough
        def hessian(self, x, lagrange, obj_factor):
            ipopt_data = x.reshape(self.model.n, (self.model.n_x + self.model.n_u), order='F')
            states_x = ipopt_data[:, 0:self.model.n_x]
            control_u = ipopt_data[:, self.model.n_x:]
            hessLagf_np = np.asarray(self.model.hessLagf(states_x, control_u, self.setpoints, obj_factor, lagrange))
            return hessLagf_np[hessLag_row, hessLag_col]


        def intermediate(
                self,
                alg_mod,
                iter_count,
                obj_value,
                inf_pr,
                inf_du,
                mu,
                d_norm,
                regularization_size,
                alpha_du,
                alpha_pr,
                ls_trials
        ):
            pass
            #
            # Example for the use of the intermediate callback.
            #
            # print("Objective value at iteration #%d is - %g" % (iter_count, obj_value))
