# AI sandbox
A collection of some personal robotic algorithm and tests

## Hybrid A star path planning
This algorithm use as basis the well-known A* algorithm to find the path with minimum cost to the goal. The heuristic 
was adjusted to focus the path towards the goal, while also reducing jerking in the steering. In order to take in
consideration the non-holonomic dynamics of a car, a kinematic bicycle model is used. Some paths are pre-computed and
used in a look-up table fashion to reduce the computational time.

![](hybrid_a_star/anim.gif)

Script: hybrid_A_star_test.py

## Non-linear Model predictive control
A non-linear model predictive controller was implemented in order to control a simulated vehicle. The kinematic bicycle
model in the Frenet-Serret reference frame was used to track a predefined path. The advante of using MPC can be noticed
by the smooth control outputs, and the constraints that can be impose in a natural way to the states. Finally, also notice
that the tracking has been designed to make the car stop at the end of the path, as can be seen in the smooth desacceleration 
and final speed of the animation.

![](mpc_kin_plots/anim.gif)

Script: mpc_kinematic_controller_f_test.py

## Requirements
The python packages required to run the tests are described in requirements.txt
It is recommended to use Anaconda to install ipopt and casadi.
Tested with Anaconda-python 3.8