import os
import glob
import sys
import subprocess
import imageio

def generate_video(folder_with_imgs):

    os.chdir(folder_with_imgs)
    subprocess.call([
        'ffmpeg', '-y', '-i', 'fig%02d.png',
        '-c:v', 'libx264',
        '-r', '30',
        '-pix_fmt', 'yuv420p',
        # '-filter:v', "setpts=2.0*PTS",
        'video_name.mp4'
    ])


def generate_gif(folder_with_imgs, filename="anim.gif"):
    os.chdir(folder_with_imgs)
    gif_path = os.path.join(folder_with_imgs, filename)
    with imageio.get_writer(gif_path, mode='I') as writer:
        for figure in sorted(glob.glob('*.png')):
            image = imageio.imread(folder_with_imgs + '/' + figure)
            writer.append_data(image)
    print("gif animation created: %r" %gif_path)

if __name__=='__main__':
    # generate_video(sys.argv[1])
    generate_gif(sys.argv[1])