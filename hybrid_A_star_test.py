"""
Hybrid A star test
author: Toshiharu Tabuchi
"""

import os, sys, glob
sys.path.insert(0, os.path.join(os.path.dirname(__file__)+'/vehicle_models'))
sys.path.insert(0, os.path.join(os.path.dirname(__file__)+'/plot_tools'))
sys.path.insert(0, os.path.join(os.path.dirname(__file__)+'/path_planning'))

from trajectories_lookup_table_generator import generate_lookup_table, compute_traj_ramifications
from car_plot import plot_car
from hybrid_a_star import *
from kinematic_model_f import kinematic_model

import numpy as np
import matplotlib.pyplot as plt

PLOT_ANIMATION = True
SAVE_PLOTS = False
if PLOT_ANIMATION:
    try:
        os.mkdir('hybrid_a_star')
    except:
        files = glob.glob('hybrid_a_star/*.png')
        for f in files:
            os.remove(f)


# ------------------------------------------------------------------------------------
# Hybrid A* parameters
# ------------------------------------------------------------------------------------
GOAL_POS = np.asarray([20, -20])
P_GOAL = 1
P_TURN  = 0.3  # penalization weight for turning. If the car turns, the cost is higher.
P_STEER = 0.8
P_BACK  = 0.1 # penalization for going backwards

N_max = 1000
V_max = N_max * 6

N_ANGLES_DIV = 10 # number of steering angles to test during the path search

nodes_list = np.ones([N_max, 10]) * np.inf

processed_nodes = []

# ------------------------------------------------------------------------------------
# Kinematic look up table generator parameters
# ------------------------------------------------------------------------------------
car_geometry = [1.25, 1.25]  # Lr, Lf
kin_model = kinematic_model(time_horizon=1, car_geometry=car_geometry, n=10, m=12)

kin_lookup_table = generate_lookup_table(speed = 1,
                                         steering_range = np.linspace(-45, 45, N_ANGLES_DIV)/180*np.pi,
                                         vehicle_model = kin_model)

# ------------------------------------------------------------------------------------
# Hybrid A* initialization
# ------------------------------------------------------------------------------------

# Add first Node (START)
#               | x | y | theta | delta | v |
xk = np.asarray([0, 0, np.pi/2, 0, 0])  # starting position
tc = 0
hc = euclidean_dist(xk[0], xk[1], GOAL_POS[0], GOAL_POS[1])

new_node = Node(tc, hc, xk, 0)
nodes_list[0,:] = new_node.data

iterations = 0
while(euclidean_dist(GOAL_POS[0], GOAL_POS[1], xk[0], xk[1]) > 2):
    print("Distance to goal pos: ", euclidean_dist(GOAL_POS[0], GOAL_POS[1], xk[0], xk[1]) )
    node_k = pop_node(nodes_list)
    processed_nodes.append(node_k)
    print("Trajectory cost: ", node_k[1], " |  Heuristic cost: ", node_k[2])
    xk = node_k[5:10]
    trajectories = compute_traj_ramifications(xk, kin_lookup_table)

    if (PLOT_ANIMATION):
        # plt.cla()
        plt.gcf().canvas.mpl_connect('key_release_event',
                                     lambda event: [exit(0) if event.key == 'escape' else None])
        plt.axis('equal')
        plt.xlim(-5, 25)
        plt.ylim(-25, 5)
        plt.plot(GOAL_POS[0], GOAL_POS[1], marker = '*', markersize = 10, color = 'magenta')
        plot_car(node_k[5], node_k[6], node_k[7], node_k[8])
        plt.title("Hybrid A star using bicycle model")
        # plt.pause(0.05)

    for traj in trajectories:
        X_final = traj[-1]  # position and orientation in at the end of each trajectory
        d_theta = (np.abs(X_final[2] - xk[2]) + 1) * P_TURN  # if the car goes straight, the cost is just P_TURN
        d_delta = P_STEER*(np.abs(X_final[3] - xk[3]))**2  # penalty for steering. Reduce jerk

        if(xk[4] < 0):
            d_back = P_BACK
        else:
            d_back = 0

        tc = d_theta + d_delta + d_back
        hc = P_GOAL*euclidean_dist(X_final[0], X_final[1], GOAL_POS[0], GOAL_POS[1])
        new_node = Node(tc, hc, X_final, node_k.data[3])
        add_node(new_node, nodes_list)
        print("Iteration ", iterations, ": Trajectory cost: ", tc, " |  Heuristic cost: ", hc)

    if (PLOT_ANIMATION):
        plt.plot(traj[:, 0], traj[:, 1])
        plt.gca().set_aspect('equal', adjustable='box')

        if SAVE_PLOTS:
            plt.savefig('hybrid_a_star' + "/fig%04d.png" % iterations)
        else:
            plt.pause(0.1)

    iterations+=1


with open('traj1.csv', 'w') as f:
    for line in processed_nodes:
        first_line = True
        for elem in line:
            if first_line:
                f.write("%s" % elem)
                first_line = False
            else:
                f.write(", %s" % elem)

        f.write("\r\n")

if (PLOT_ANIMATION):
    plt.show()