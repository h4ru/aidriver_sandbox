"""
Vehicle trajectory generator for look up table
author: Toshiharu Tabuchi
"""

import os, sys
sys.path.insert(0, os.path.join(os.path.dirname(__file__)+'/vehicle_models'))

from kinematic_model_f import kinematic_model

import casadi as ca
import numpy as np
import matplotlib.pyplot as plt
import time
import math


# TODO: also add steering angle in generated trajectories
#########################################################################################

def Rot_Matrix2D(yaw):
    return np.array([[math.cos(yaw), math.sin(yaw)],
                     [-math.sin(yaw), math.cos(yaw)]])

def rotate_vectors(x_y_theta, rot_angle):
    assert np.size(x_y_theta,1) == 3, "Array must have 3 columns: x, y, yaw"
    N = np.size(x_y_theta, 0)
    xy = x_y_theta[:, 0:2].dot(Rot_Matrix2D(rot_angle))
    yaw = x_y_theta[:,2] + rot_angle
    yaw = yaw.reshape(N,1)

    return np.concatenate((xy, yaw), axis=1)

def compute_traj_ramifications(x_y_theta_0, kin_lookup_table):
    new_trajectories = []
    N = np.size(kin_lookup_table[0], 0 )
    xyt = np.zeros([N, 5])

    for traj in kin_lookup_table:
        xyt[:, 0:3] = rotate_vectors(traj[:, 0:3], rot_angle=x_y_theta_0[2] - np.pi / 2)
        xyt[:, 0] = xyt[:, 0] + x_y_theta_0[0]
        xyt[:, 1] = xyt[:, 1] + x_y_theta_0[1]
        xyt[:, 3] = traj[:, 5]
        xyt[:, 4] = traj[:, 6]
        new_trajectories.append(xyt.copy())

    return new_trajectories.copy()

# ------------------------------------------------------------------------------------
# Kinematic model lookup table generation
# Here, some predetermine final states are pre-computed to save time
# during the initial exploration with A*
# ------------------------------------------------------------------------------------

def generate_lookup_table(speed, steering_range, vehicle_model):

    speeds = [+speed, -speed]  # generate trajectories of the car going forward and backwards
    N_ticks = vehicle_model.n+1 # simulation time = time_horizon/N*N_ticks
    # constant_speed = 1
    # steering_deltas = np.asarray([-45, 0, 45])/180*np.pi


    # vehicle_model = kinematic_model(time_horizon, car_geometry, dummy_weights_kin, n=N, m=m)

    u0 = [0, 0] #    delta_dot_0, jerk_0
    dummy_setpoints = ca.vertcat(0, 0, 0, 0, 0, 0, 0)
    kin_sim_data = np.zeros([N_ticks, vehicle_model.n_x])

    kin_lookup_table = []  # store the whole trajectory

    for v in speeds:

        for delta in steering_range:
            # states = ca.vertcat(pos_x, pos_y, theta, xte, theta_e, delta, v, accel, s)
            xk = [0, 0, np.pi / 2, 0, 0, delta, v, 0, 0]

            for tick in range(N_ticks - 1):
                Fk = vehicle_model.RK4_int(x0=xk, control=u0, setpoints=dummy_setpoints)
                xk = kin_sim_data[tick + 1, :] = ca.transpose(Fk['xf'])

            plt.plot(kin_sim_data[:, 0], kin_sim_data[:, 1])

            kin_lookup_table.append(kin_sim_data[:,0:7].copy())

    return kin_lookup_table



if __name__ == '__main__':

    kin_lookup_table = generate_lookup_table(time_horizon=1,
                                             speed=1,
                                             steering_range=np.asarray([-30, 0, 30]) / 180 * np.pi)

    # Initialization
    trajectories_tree = []
    tree_upper_layer = []  # latest layer in the trajectories_tree
    tree_upper_layer.append(kin_lookup_table.copy()) # append the first trajectories ramification
    N_max = 3**4-1

    start = time.time()

    while len(tree_upper_layer) < N_max:
        trajectories = tree_upper_layer.pop(0)

        for traj in trajectories:
            xy_final = traj[-1]
            traj_temp = compute_traj_ramifications(xy_final, kin_lookup_table)
            trajectories_tree.append( traj_temp)
            tree_upper_layer.append(traj_temp)

    end = time.time()
    print("Elapse time for N_max = ", N_max, ": ", end - start)


    #plot tree
    for trajectories in trajectories_tree:
        for traj in trajectories:
            plt.plot(traj[:, 0], traj[:, 1])

    plt.gca().set_aspect('equal', adjustable='box')
    plt.show()