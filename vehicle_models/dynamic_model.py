"""
Dynamic vehicle model
Calculation of cross track and yaw error in Frenet frame
author: Toshiharu Tabuchi
"""

import casadi as ca
import numpy as np

# ------------------------------------------------------------------------------------

class dynamic_model:
    
    def __init__(self, th, car_description, weights, n=10, m=1):
        self.n = n
        self.m = m  # RK4 steps per shot

        # Distance between center of rotation to the rear and front
        [Lr, Lf, Cr, Cf, k_drag, car_mass, I_z] = car_description

        # Cost function tuning weights

        # kXte = ca.SX.sym('kXte')
        # kTheta_e = ca.SX.sym('kTheta_e')
        #
        # kSteer = ca.SX.sym('kSteer')
        # kDeltadot = ca.SX.sym('kDeltadot')
        # kSpeed = ca.SX.sym('kSpeed')
        #
        # qThrottle = ca.SX.sym('qThrottle')
        # qTheta_dot = ca.SX.sym('qTheta_dot')
        #
        # # Terminal weights
        # pXte = ca.SX.sym('pXte')
        # pTheta_e = ca.SX.sym('pTheta_e')

        # weights = ca.vertcat(kXte, kTheta_e, kSteer, kDeltadot, kSpeed, qThrottle, qTheta_dot, pXte, pTheta_e)

        [kXte, kTheta_e, kSteer, kDeltadot, kSpeed, qThrottle, qTheta_dot, pXte, pTheta_e] = weights


        # Position of the car in the world reference
        pos_x = ca.SX.sym('x')
        pos_y = ca.SX.sym('y')
        theta = ca.SX.sym('theta')
        theta_dot = ca.SX.sym('theta_dot')
        beta = ca.SX.sym('beta')
        delta = ca.SX.sym('delta')
        v = ca.SX.sym('v')
        xte = ca.SX.sym('xte') # Cross track error in frenet frame
        theta_e = ca.SX.sym('theta_e') # theta error in frenet frame

        states = ca.vertcat(pos_x, pos_y, v, theta, beta, theta_dot, xte, theta_e, delta)

        # Control Inputs
        deltadot = ca.SX.sym('deltadot')
        throttle = ca.SX.sym('throttle')
        
        control = ca.vertcat(deltadot, throttle)
        
        # Setpoints variables (curve and cruise speed)
        # Polynomial coeffs
        a = ca.SX.sym('a')
        b = ca.SX.sym('b')
        c = ca.SX.sym('c')
        d = ca.SX.sym('d')
        v_cruise = ca.SX.sym('cruise speed setpoint')

        setpoints = ca.vertcat(a, b, c, d, v_cruise)
        

        # --------------------------------------------------------------
        # Definition of Model differential equations
        # --------------------------------------------------------------

        # Curvature for 3rd degree polynomial
        # v = np.sqrt(vx**2 + vy**2)
        epsilon = 0.00001
        k = (6*a*pos_y + 2*b) / ca.power(ca.sqrt(1 + ca.power(3*a*pos_y**2 + 2*b*pos_y + c, 2)), 3)
        s_dot = (v/ca.cos(beta)/(1-d*k)) * ca.cos(theta_e)
        # beta = ca.arctan(Lr / (Lr + Lf) * ca.tan(delta))

        # tyre forces
        alpha_f = delta - ca.arctan((v*ca.tan(beta) + theta_dot*Lf) / (v + epsilon))
        alpha_r = -ca.arctan((v * ca.tan(beta) - theta_dot*Lr) / (v + epsilon))
        # alpha_f = (delta - beta + Lf*theta_dot/(v))
        # alpha_r = (- beta + Lr*theta_dot/(v))
        Fy_f = Cf * alpha_f
        Fy_r = Cr * alpha_r

        # pos_x, pos_y, v, theta, beta, theta_dot, xte, theta_e, delta
        statesdot = ca.vertcat( v * ca.cos(theta + beta) / ca.cos(beta),
                                v * ca.sin(theta + beta) /ca.cos(beta),
                                throttle,
                                theta_dot,
                                (2 / car_mass / (v+epsilon) ) * (Fy_f*ca.cos(delta) + Fy_r) + theta_dot,
                                (2/I_z)*(+Lf*Fy_f*ca.cos(delta) - Lr*Fy_r),
                                -v / ca.cos(beta) * ca.sin(theta_e + beta),
                                v / ca.cos(beta) / (Lr+Lf) * ca.sin(beta) - s_dot * k,
                                deltadot)

        # Objective term
        L = kXte*xte**2 + kTheta_e*theta_e**2 + kSteer * delta ** 2 + kDeltadot * deltadot ** 2 + kSpeed*(v-v_cruise)**2 +\
            qThrottle * throttle ** 2+ qTheta_dot*theta_dot**2

        f = ca.Function('f', [states, control, setpoints], [statesdot, L])

        # Terminal cost (different terminal weights)
        Lf = pXte*xte**2 + pTheta_e*theta_e**2
        ft = ca.Function('ft', [states, control, setpoints], [statesdot, Lf])
    
       
    # --------------------------------------------------------------------------------------
    # Defining NLP
    # --------------------------------------------------------------------------------------
        
        self.n_x = n_x = states.shape[0]
        self.n_u = n_u = control.shape[0]
        self.n_vars = (n_x +n_u) * n  # total number of variables after discretization 
        n_c =  setpoints.shape[0]
        
        # RK4 integrator
        dt = th / n
        self.RK4_int = self.RK4_integrator(f, [n_x, n_u, n_c], dt, m)
        

        # Start with an empty NLP
        w = []
        w0 = []
        lbw = []
        ubw = []
        x = []
        u = []
        J = 0
        g = []
        lbg = []
        ubg = []

        # define symbolic limits and bounds
        lbx = ca.MX.sym('lbx', self.n_x)
        ubx = ca.MX.sym('ubx', self.n_x)
        x0 = ca.MX.sym('x0', self.n_x)
        xk_end = 0

        lbu = ca.MX.sym('lbu', self.n_u)
        ubu = ca.MX.sym('ubu', self.n_u)
        u0  = ca.MX.sym('u0', self.n_u)


        # "Lift" initial conditions
        xk = ca.SX.sym('x0', self.n_x)
        w.append(xk)  # w hold control and states for ipopt
        lbw.append(x0)
        ubw.append(x0)
        w0.append(x0)
        x.append(xk)
        
        for k in range(n-1):
            # New NLP variable for the control
            uk = ca.SX.sym('U_' + str(k), self.n_u)
            w.append(uk)
            lbw.append(lbu)
            ubw.append(ubu)
            w0.append(u0)
            u.append(uk)

            Fk_end = self.RK4_int(x0=xk, control=uk, setpoints=setpoints)
            xk_end = Fk_end['xf']
            J = J + Fk_end['qf']

            xk = ca.SX.sym('x_' + str(k + 1), n_x)
            w.append(xk)
            lbw.append(lbx)
            ubw.append(ubx)
            w0.append(x0)
            x.append(xk)

            # Add equality constraint
            g.append(xk_end - xk)
            lbg.append(np.zeros(n_x))
            ubg.append(np.zeros(n_x))

        # Add terminal cost
        x_t, q_t = ft(xk_end, uk, setpoints)
        J = J + q_t


        # ---------------------------------------------------------------------------
        # Definition of:
        # cost function
        # Gradient of cost function
        # Jacobian of inequalities
        # ---------------------------------------------------------------------------

        w = ca.vertcat(*w)
        g = ca.vertcat(*g)
        x = ca.horzcat(*x)
        u = ca.horzcat(*u)
        w0 = ca.vertcat(*w0)
        lbw = ca.vertcat(*lbw)
        ubw = ca.vertcat(*ubw)


        self.lbwF = ca.Function('lbw', [x0, lbx, lbu], [lbw])
        self.ubwF = ca.Function('lbw', [x0, ubx, ubu], [ubw])
        self.w0F = ca.Function('w0', [x0, u0], [w0])
        self.lbg = np.concatenate(lbg)
        self.ubg = np.concatenate(ubg)

        # total number of variables
        self.n_vars = w.shape[0]

        # total number of constraints
        self.n_constraints = g.shape[0]

        # Cost function template
        self.Jf = ca.Function('Jf', [w, setpoints], [J], ['vars', 'setpoints'], ['cost'])

        # Gradient of cost function
        gradJ = ca.gradient(J, w)
        self.gradJf = ca.Function('gradJf', [w, setpoints], [gradJ], ['vars', 'setpoints'], ['gradient'])

        # Constraints
        self.Gf = ca.Function('Gf', [w, setpoints], [g], ['vars', 'setpoints'], ['constraints'])

        # Jacobian of constraints
        self.jacobianG = ca.jacobian(g, w)
        self.jacobianGf = ca.Function('jacobianGf', [w, setpoints], [self.jacobianG], ['vars', 'setpoints'],
                                      ['jacobian'])

        # mapping function from w contanated vector to x and u
        self.mappingF = ca.Function('mapping_w_to_x_u', [w], [x, u], ['w'], ['x', 'u'])

        # Hessian of the Lagrangian
        # sigma = ca.SX.sym('sigma');
        # ip_lambda = ca.SX.sym('lambda', self.n_constraints)
        # hesscontr = 0
        #
        # hessob = ca.jacobian(gradJ, w) * sigma
        # for i in range(self.n_constraints):
        #     temp_jacobian = ca.jacobian(g[i], w)
        #     hesscontr = hesscontr + ip_lambda[i] * ca.jacobian(temp_jacobian, w)
        #
        # self.hessLag = ca.tril(hessob + hesscontr)  # Ipopt solo requiere el lado izquierdo inferior
        # self.hessLagf = ca.Function('hessLagf', [w, setpoints, sigma, ip_lambda], [self.hessLag],
        #                             ['vars', 'setpoints', 'sigma', 'ip_lambda'], ['hessLag'])
        
    # ------------------------------------------------------------------------------------

    # def get_nlp(self):
    #
    #     return self.Jf, \
    #            self.gradJf, \
    #            self.Gf, \
    #            self.jacobianGf, \
    #            self.jacobianG, \
    #            self.hessLagf, \
    #            self.hessLag, \
    #            self.mappingF, \
    #            self.lbwF, \
    #            self.ubwF, \
    #            self.w0F, \
    #            np.concatenate(self.lbg), \
    #            np.concatenate(self.ubg), \
    #            self.n_vars, \
    #            self.n_constraints
    # ------------------------------------------------------------------------------------


    def generate_C_code(self):

        opts = dict(main = False, with_header=False)
        self.Jf.generate('Jf.c', opts)
        self.gradJf.generate('gradJf.c', opts)
        self.Gf.generate('Gf.c', opts)
        self.jacobianGf.generate('jacobianGf.c', opts)


    # ------------------------------------------------------------------------------------


    def RK4_integrator(self, func, inputs_sizes, dt, m):
        cost = 0
        h = dt/m
        [n_x, n_u, n_c] = inputs_sizes

        x0 = ca.SX.sym('X0', n_x)
        u = ca.SX.sym('U', n_u)
        
        x = x0 # initializing x on x0
        constants = ca.SX.sym('constants', n_c)

        for i in range(m):
            k1, k1_q = func(x, u, constants)
            k2, k2_q = func(x + h / 2 * k1, u, constants)
            k3, k3_q = func(x + h / 2 * k2, u, constants)
            k4, k4_q = func(x + h * k3, u, constants)

            x = x + h / 6 * (k1 + 2 * k2 + 2 * k3 + k4)
            cost = cost + h / 6 * (k1_q + 2 * k2_q + 2 * k3_q + k4_q)

        return ca.Function('f', [x0, u, constants], [x, cost],
                        ['x0', 'control', 'setpoints'], ['xf', 'qf'])


# ------------------------------------------------------------------------------------
# Small test
# ------------------------------------------------------------------------------------

if __name__ == '__main__':

    time_horizon = 2  # seconds
    N = 10
    m = 12

    a = -0.00019303984632064786
    b = -0.00982937496151641
    c = -0.009073049411268236
    d = 0.23114778016022308

    poly_coeffs = [a, b, c, d]

    kXte = 1
    kTheta_e = 1
    kSteer = 2
    kDeltadot = 1
    kSpeed = 1
    qThrottle = 1
    qTheta_dot = 1

    pXte = 10
    pTheta_e = 10

    weights = [kXte, kTheta_e, kSteer, kDeltadot, kSpeed, qThrottle, qTheta_dot, pXte, pTheta_e]

    Lr = 1.5
    Lf = 1.5
    Cr = 55e3 # N/rad
    Cf = 90e3
    k_drag = 0.25
    car_mass = 2090
    Inertia_z = car_mass*((Lr+Lf)/2)**2 # approximation
    car_description = [Lr, Lf, Cr, Cf, k_drag, car_mass, Inertia_z]

    # servo-steering limits
    delta_angle_limit = (30 / 180) * np.pi  # degrees
    delta_rate_limit = (15 / 180) * np.pi  # degrees per second


    model = dynamic_model(time_horizon,
                            car_description,
                            weights,
                            n = N,
                            m = m)

    N_ticks = 50
    # pos_x, pos_y, v, theta, beta, theta_dot, xte, theta_e, delta
    pos_x_0 = 0  # f(y) when y = 0
    pos_y_0 = 0
    v_0 = 5
    theta_0 = np.pi / 2  # car looking ahead
    beta_0 = 0
    theta_dot_0 = 0
    xte_0 = +d
    theta_e_0 = -theta_0 + np.arctan2(1, c)
    delta_0 = 30/180*np.pi
    delta_dot_0 = 0
    throttle_0 = 0

    X_t1 = np.zeros([N_ticks, model.n_x])
    alpha_f = np.zeros([N_ticks, 1])
    alpha_r = np.zeros([N_ticks, 1])
    beta = np.zeros([N_ticks, 1])

    alpha_f[0] = 0
    alpha_r[0] = 0
    beta[0] = 0
    x0 = X_t1[0, :] = [pos_x_0, pos_y_0, v_0, theta_0, beta_0, theta_dot_0, xte_0, theta_e_0, delta_0]
    u0 = [delta_dot_0, throttle_0]

    dummy_setpoints = ca.vertcat(0, 0, 0, 0, 0)


    # --------------------------------------------------------------------------------------
    # Simulate vehicle model
    # --------------------------------------------------------------------------------------

    for tick in range(N_ticks-1):
        Fk = model.RK4_int(x0=x0, control=u0, setpoints=dummy_setpoints)
        x0 = X_t1[tick+1, :] = ca.transpose(Fk['xf'])

        v = x0[3]
        delta = x0[8]
        theta_dot = x0[5]
        epsilon = 1e-7
        beta = x0[4]
        alpha_f[tick+1] = ((v*np.tan(beta) + theta_dot*Lf))
        alpha_r[tick + 1] = ((v*np.tan(beta) - theta_dot*Lf) )
        # alpha_f[tick+1] = delta - np.arctan((v*np.tan(beta) + theta_dot*Lf) / (v + epsilon))
        # alpha_r[tick + 1] = - np.arctan((v*np.tan(beta) - theta_dot*Lf) / (v + epsilon))

    # pos_x, pos_y, v, theta, beta, theta_dot, xte, theta_e, delta
    pos_x = X_t1[:,0]
    pos_y = X_t1[:,1]
    v = X_t1[:,2]
    theta = X_t1[:,3]
    beta = X_t1[:,4]
    theta_dot = X_t1[:,5]
    xte = X_t1[:,6]
    theta_e = X_t1[:,7]
    delta = X_t1[:,8]


    # --------------------------------------------------------------------------------------
    # Plot simulation
    # --------------------------------------------------------------------------------------

    from matplotlib.pyplot import subplots, plot, step, figure, legend, show, close, axis
    plot(pos_x, pos_y, '--b', label='car trajectory')
    axis('equal')
    legend()

    fig1, (ax1, ax2, ax3, ax4, ax5) = subplots(5,1)
    ax1.plot(np.arange(N_ticks)*(time_horizon/N), v, 'bs', label='vx')
    ax1.legend(shadow=True, fancybox=True)
    ax2.plot(np.arange(N_ticks)*(time_horizon/N), theta, 'r^', label='vy')
    ax2.legend(shadow=True, fancybox=True)
    ax3.step(np.arange(N_ticks)*(time_horizon/N),delta/np.pi*180, where='post', label='delta')
    ax3.legend(shadow=True, fancybox=True)
    ax4.step(np.arange(N_ticks)*(time_horizon/N),theta/np.pi*180, where='post', label='theta')
    ax4.legend(shadow=True, fancybox=True)
    ax5.step(np.arange(N_ticks)*(time_horizon/N),theta_dot/np.pi*180, where='post', label='theta_dot')
    ax5.legend(shadow=True, fancybox=True)


    fig2, (ax6, ax7, ax8, ax9) = subplots(4,1)
    ax6.plot(np.arange(N_ticks)*(time_horizon/N), alpha_r/np.pi*180, 'bs', label='alpha rear')
    ax6.legend(shadow=True, fancybox=True)
    ax7.plot(np.arange(N_ticks)*(time_horizon/N), alpha_f/np.pi*180, 'r^', label='alpha front')
    ax8.legend(shadow=True, fancybox=True)
    ax8.plot(np.arange(N_ticks)*(time_horizon/N), beta/np.pi*180, 'r^', label='beta')
    ax8.legend(shadow=True, fancybox=True)
    ax9.step(np.arange(N_ticks)*(time_horizon/N),theta_dot/np.pi*180, where='post', label='theta_dot')
    ax9.legend(shadow=True, fancybox=True)
    show()

    print(model.jacobianGf)
    model.generate_C_code()