"""
Kinematic vehicle model
Calculation of cross track and yaw error in the Frenet frame
author: Toshiharu Tabuchi
"""

import casadi as ca
import numpy as np

# ------------------------------------------------------------------------------------

class kinematic_model:
    
    def __init__(self, time_horizon, car_geometry, cost_weights=np.zeros([10, 1]), n=10, m=1):
        self.n = n
        self.m = m

        # Distance between center of rotation to the rear and front
        Lr = car_geometry[0]
        Lf = car_geometry[1]

        # Cost function tuning weights
        [kSteer, kDeltadot, kSpeed, kThrottle, qxte, qtheta_e, Pxte, Ptheta_e, Pvf, Psf] = cost_weights


        # Deviation of the car position to the reference path
        pos_x = ca.SX.sym('x')
        pos_y = ca.SX.sym('y')

        # rotation of the car
        theta = ca.SX.sym('theta')

        # Steering angle
        delta = ca.SX.sym('delta')

        # Speed of the car
        v = ca.SX.sym('v')

        accel = ca.SX.sym('acceleration')

        # Arc lenght's change of rate
        s = ca.SX.sym('s')

        # Cross track error
        xte = ca.SX.sym('xte')

        # theta error
        theta_e = ca.SX.sym('theta_e')

        # Control Inputs
        # change of rate of steering angle (control variable)
        deltadot = ca.SX.sym('deltadot')
        jerk = ca.SX.sym('jerk')

        states = ca.vertcat(pos_x, pos_y, theta, xte, theta_e, delta, v, accel, s)
        control = ca.vertcat(deltadot, jerk)

        # Setpoints variables (curve and cruise speed)
        # Polynomial coeffs
        a = ca.SX.sym('a')
        b = ca.SX.sym('b')
        c = ca.SX.sym('c')
        d = ca.SX.sym('d')

        v_cruise = ca.SX.sym('cruise speed setpoint')
        vf = ca.SX.sym('Final speed')
        sf = ca.SX.sym('Total driven distance wanted')

        setpoints = ca.vertcat(a, b, c, d, v_cruise, vf, sf)

        self.n_x = states.shape[0]
        self.n_u = control.shape[0]
        self.n_vars = (self.n_x + self.n_u) * n  # total number of variables after discretization

        # --------------------------------------------------------------
        # Definition of NLP symbolic variables for discretization
        # --------------------------------------------------------------

        # Create NLP variables for states
        X = ca.SX.sym('X', n, states.shape[0])
        U = ca.SX.sym('U', n, control.shape[0])

        # Inputs of the Hessian of the Lagrangian
        sigma = ca.SX.sym('sigma')  # Ipopt
        ip_lambda = ca.SX.sym('lambda', (n - 1) * self.n_x)  # Ipopt

        hInput = [X, U, setpoints, sigma, ip_lambda]
        hNames = ['states', 'controls', 'setpoints', 'sigma', 'ip_lambda']

        # Model equations

        # Curvature for 3rd degree polynomial
        k = (6*a*pos_y + 2*b) / ca.power(ca.sqrt(1 + ca.power(3*a*pos_y**2 + 2*b*pos_y + c, 2)), 3)
        delta = -delta
        beta = ca.arctan(Lr / (Lr + Lf) * ca.tan(delta))
        theta_dot = v / Lr * ca.sin(beta) # berkeley
        # theta_dot = v / (Lr + Lf) * ca.cos(beta) * ca.tan(delta)  # coursera
        s_dot = (1/(1-xte*k)*(v*ca.cos(theta_e) - Lr*theta_dot*ca.sin(theta_e)))


        # states = ca.vertcat(pos_x, pos_y, theta, xte, theta_e, delta, v, accel, s)
        statesdot = ca.vertcat(v * ca.cos(theta + beta),
                            v * ca.sin(theta + beta),
                            theta_dot,
                            -(v*ca.sin(theta_e) + (Lr*theta_dot*ca.cos(theta_e))),
                            (theta_dot - s_dot * k),
                            deltadot,
                            accel,
                            jerk,
                            s_dot )

        # Objective term
        L = qxte*xte**2 + qtheta_e*theta_e**2 + kSteer * delta ** 2 + \
            kDeltadot * deltadot ** 2 + kThrottle * accel ** 2 \
            + jerk**2 \
            + kSpeed*(v-v_cruise)**2

        f = ca.Function('f', [states, control, setpoints], [statesdot, L])

        # Terminal cost (different terminal weights)
        Lf = Pxte*xte**2 + Ptheta_e*theta_e**2 + Pvf*(v-vf)**2 + Psf*(s-sf)**2
        ft = ca.Function('ft', [states, control, setpoints], [statesdot, Lf])

        # ---------------------------------------------------------------------
        # Runge Kutta Rk4 with fixed step
        # Integrate Cost function and estimate states at different step points
        # ---------------------------------------------------------------------

        states0 = ca.SX.sym('X0', states.size1())
        control0 = ca.SX.sym('U0', control.size1())

        dt = time_horizon / n
        self.RK4_int = self.RK4_integrator(f, [states0, control0, setpoints], dt, m)
        RK4_int_t = self.RK4_integrator(ft, [states0, control0, setpoints], dt, m)

        # ---------------------------------------------------------------------------
        # Integration of the cost function and states across different time steps
        # using the Multiple shooting approach
        # ---------------------------------------------------------------------------

        J = 0
        g = ca.SX.sym('g', n - 1, states.shape[0])

        for i in range(n-1):
            Xk = X[i, :]
            Uk = U[i, :]
            Fk = self.RK4_int(x0=Xk, control=Uk, setpoints=setpoints)
            J = J + Fk['qf']  # the last point is not integrated
            Xk_end = ca.transpose(Fk['xf'])
            g[i, :] = Xk_end - X[i + 1, :]

        Fk = RK4_int_t(x0=X[n - 1, :], control=U[n - 1, :], setpoints=setpoints)
        J = J + Fk['qf']  # The last point is integrated here

        # ---------------------------------------------------------------------------
        # Definition of:
        # cost function
        # Gradient of cost function
        # Jacobian of inequalities
        # ---------------------------------------------------------------------------

        # Cost function template
        self.Jf = ca.Function('Jf', [X, U, setpoints], [J], ['states', 'control_inputs', 'setpoints'], ['cost'])

        # Gradient of cost function
        gradJ = ca.gradient(J, ca.transpose(ca.vertcat(ca.transpose(X), ca.transpose(U))))
        self.gradJf = ca.Function('gradJf', [X, U, setpoints], [gradJ], ['states', 'control_inputs', 'setpoints'], ['gradient'])

        # Constraints
        self.Gf = ca.Function('Gf', [X, U, setpoints], [g], ['states','control_inputs', 'setpoints'], ['constraints'])

        # Jacobian of constraints
        self.jacobianG = ca.jacobian(g, ca.transpose(ca.vertcat(ca.transpose(X), ca.transpose(U))))
        self.jacobianGf = ca.Function('jacobianGf', [X, U, setpoints], [self.jacobianG], ['states', 'control_inputs', 'setpoints'], ['jacobian'])

        # Hessian of the Lagrangian
        hesscontr = 0
        X_U = ca.transpose(ca.vertcat(ca.transpose(X), ca.transpose(U)))

        hessob = ca.jacobian(gradJ, X_U) * sigma
        for i in range(g.shape[0]):
            for j in range(g.shape[1]):
                temp_jacobian = ca.jacobian(g[i, j], X_U)
                hesscontr = hesscontr + ip_lambda[i*g.shape[1] + j] * ca.jacobian(temp_jacobian, X_U)

        self.hessLag = ca.tril(hessob+hesscontr) # Ipopt solo requiere el lado izquierdo inferior
        self.hessLagf = ca.Function('hessLagf', hInput, [self.hessLag], hNames, ['hessLag'] )
    # ------------------------------------------------------------------------------------


    def generate_C_code(self):

        opts = dict(main = False, with_header=False)
        self.Jf.generate('Jf.c', opts)
        self.gradJf.generate('gradJf.c', opts)
        self.Gf.generate('Gf.c', opts)
        self.jacobianGf.generate('jacobianGf.c', opts)


    # ------------------------------------------------------------------------------------


    def RK4_integrator(self, func, inputs, dt, steps):
        cost = 0
        h = dt/steps

        x = inputs[0]
        u = inputs[1]
        constants = inputs[2]

        for i in range(steps):
            k1, k1_q = func(x, u, constants)
            k2, k2_q = func(x + h / 2 * k1, u, constants)
            k3, k3_q = func(x + h / 2 * k2, u, constants)
            k4, k4_q = func(x + h * k3, u, constants)

            x = x + h / 6 * (k1 + 2 * k2 + 2 * k3 + k4)
            cost = cost + h / 6 * (k1_q + 2 * k2_q + 2 * k3_q + k4_q)

        return ca.Function('f', [inputs[0], inputs[1], inputs[2]], [x, cost],
                        ['x0', 'control', 'setpoints'], ['xf', 'qf'])



# ------------------------------------------------------------------------------------
# Small test
# ------------------------------------------------------------------------------------

if __name__ == '__main__':
    n = 5
    m = 5
    time_horizon = 5  # seconds

    kSteer = 1
    kDeltadot = 1
    kSpeed = 0.1
    kThrottle = 1
    qxte = 10
    qtheta_e = 1
    Pxte = 10
    Ptheta_e = 10
    Pvf = 10
    Psf = 10

    cost_weights = [kSteer, kDeltadot, kSpeed, kThrottle, qxte, qtheta_e, Pxte, Ptheta_e, Pvf, Psf]

    a = -0.00019303984632064786
    b = -0.00982937496151641
    c = -0.009073049411268236
    d = 0.23114778016022308

    Lr = 1.5
    Lf = 1.5

    # servo-steering limits
    delta_angle_limit = (30 / 180) * np.pi  # degrees
    delta_rate_limit = (15 / 180) * np.pi  # degrees per second

    car_dimensions = [Lr, Lf]
    poly_coeffs = [a, b, c, d]


    model = kinematic_model(time_horizon,
                            car_dimensions,
                            cost_weights,
                            n,
                            m)

    print(model.jacobianGf)
    model.generate_C_code()