## Node structure: | gc | tc | hc | id | prev_id | x | y | theta | delta | v |

import numpy as np

N_VARS = 10

# tc: total cost
# hc: heuristics cost
class Node:
    id = -1
    def __init__(self, tc, hc,Xf, prev_id, id=None):
        Node.id +=1
        self.id = Node.id
        self.data = np.zeros(N_VARS)

        temp_id = 0
        if id:
            temp_id = id
        else:
            temp_id = Node.id

        self.data = [tc + hc, tc, hc, temp_id, prev_id, Xf[0], Xf[1], Xf[2], Xf[3], Xf[4]]


def add_node(new_node, nodes_list):
    # find vertex with min global cost gc
    INDEX_TO_SORT = 0
    index_min = 0
    temp = np.argwhere(new_node.data[INDEX_TO_SORT] < nodes_list[:,INDEX_TO_SORT])

    if not temp.size == 0:
        index_min = temp.min()
        nodes_list[index_min + 1:, :] = nodes_list[index_min:-1, :]
        nodes_list[index_min, :] = new_node.data.copy()
    else:
        nodes_list[index_min + 1:, :] = nodes_list[index_min:-1, :]
        nodes_list[index_min, :] = new_node


def pop_node(nodes_list):
    temp = np.copy(nodes_list[0,:])
    nodes_list[:-1,:] = nodes_list[1:,:]
    nodes_list[-1, :] = np.ones([1,N_VARS])*np.inf
    return temp


def euclidean_dist(x1, y1, x2, y2):
    return np.sqrt( (x1-x2)**2 + (y1-y2)**2)



if __name__ == '__main__':
    N_max = 100
    V_max = N_max * 6

    nodes_list = np.ones([N_max, 10]) * np.inf

    # tc, hc,Xf, prev_id, id=None:
    xk = np.asarray([0, 0, np.pi/2, 0, 0])

    new_node = Node(N_VARS, 200, xk, 0)
    add_node(new_node, nodes_list)

    new_node = Node(N_VARS, 220, xk, 0)
    add_node(new_node, nodes_list)

    print(nodes_list[0:3,:])

    print(pop_node(nodes_list))
    print(pop_node(nodes_list))